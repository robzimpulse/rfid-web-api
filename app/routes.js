var User = require('../app/models/user');
var Record = require('../app/models/record');
var Rfid = require('../app/models/rfid');
var Neraca = require('../app/models/neraca');
var NeracaType = require('../app/models/neracatype');

module.exports = function(app, passport) {

    app.get('/', isLoggedIn,function(req, res) {
        res.render('index.ejs', {user : req.user, content: 'dashboard'});
    });

    app.get('/timeline', isLoggedIn,function(req, res) {
        res.render('timeline.ejs', {user : req.user, content: 'timeline'});
    });

    app.get('/neraca', isLoggedIn, function(req, res) {
        var path = Object.keys(Neraca.schema.paths);
        var data = [];
        path.splice(path.length-2,path.length-1);
        Neraca.find({user:req.user._id})
            .populate('type')
            .exec(function(err,neracas){
                if(err){
                    console.log(err);
                }
                neracas.forEach(function(neraca){
                    data.push(neraca);
                });
                res.render('neraca.ejs', {user : req.user, content: 'neraca', path: path, data: data});
        });
    });

    app.post('/neraca', function(req, res) {
        console.log(req.body);
    });

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/login');
    });

    app.get('/login', function(req, res) {
        res.render('login.ejs', { message: req.flash('loginMessage') });
    });

    app.get('/signup', function(req, res) {
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });

    app.get('/profile', isLoggedIn,function(req, res) {
        var userid = req.user._id;
        User
            .findOne({ _id : userid})
            .populate('local.deviceSerialNumber')
            .exec(function(err,user){
                if(err)
                    return res.send(err);
                if(user == null)
                    return res.send({ error : 'Not Found', message: 'User Not Found' });
                var options = {
                    path: 'local.deviceSerialNumber.records',
                    model: 'Record'
                };
                User.populate(user, options,function(err, doc){
                    if(err){
                        console.log(err);
                    }
                    return res.render('profile.ejs', {user : doc, content: 'profile'});
                });
            });
    });

    app.post('/profile', isLoggedIn,function(req, res) {
        var id = req.user._id;
        User.findOne({ _id : id },function(err,user){
            if(err)
                console.log(err);
            if(user == null)
                console.log('User not found');
            if(req.body.name != '')
                user.local.name = req.body.name;
            if(req.body.password != '')
                user.local.password = user.generateHash(req.body.password);
            user.save();
            return res.redirect('/profile');
        });
    });

    app.post('/api', function(req, res, next) {
        var body = req.body;
        User
            .findOne({ _id : body.userid })
            .exec(function(err,user){
                if(err)
                    console.log(err);
                if(user == null){
                    console.log({ error : 'Not Found', message: 'User Not Found' });
                    return res.send({ error : 'Not Found', message: 'User Not Found' });
                }
                Rfid
                    .findOne({ serialNumber : body.deviceSerialNumber })
                    .exec(function(err,rfid){
                        if(err)
                            console.log(err);
                        if(rfid == null){
                            console.log({ error : 'Not Found', message: 'Device Not Registered' });
                            return res.send({ error : 'Not Found', message: 'Device Not Registered' });
                        }
                        record = new Record();
                        record.rfidCardNumber = body.rfid ;
                        record.save();
                        rfid.records.push(record._id);
                        rfid.save();
                        console.log('['+user.local.name+']['+body.deviceSerialNumber+']['+body.rfid+']['+new Date(Date.now())+']');
                        return res.send({
                            status: 'success',
                            message: 'Data Recorded',
                            time: new Date(Date.now())
                        });
                    });
            });

    });

    app.post('/device', isLoggedIn,function(req, res) {
        var id = req.user._id;
        User.findOne({ _id : id },function(err,user){
            if(err)
                console.log(err);
            if(user == null) {
                console.log('User not found');
            }else{
                if(req.body.deviceId != '') {
                    var rfid = new Rfid();
                    rfid.serialNumber = req.body.deviceId;
                    rfid.save();
                    user.local.deviceSerialNumber.push(rfid._id);
                    user.save();
                }
            }
            return res.redirect('/profile');
        });
    });

    app.post('/device/:id', isLoggedIn,function(req, res) {
        Rfid.findOneAndRemove({ _id: req.params.id },function(err,doc){
            if(err)
                console.log(err);
            Record
                .find({_id : {$in: doc.records}})
                .remove()
                .exec(function(err,data){
                    if(err)
                        console.log(err);
                });
            console.log(doc);
            res.redirect('/profile');
        });
    });

    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/',
        failureRedirect : '/login',
        failureFlash : true
    }));

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/',
        failureRedirect : '/signup',
        failureFlash : true
    }));

};

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/login');
}