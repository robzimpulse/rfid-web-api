var mongoose            =   require('mongoose');
var Schema              =   mongoose.Schema;

var schema              =   new Schema({
    description         :   {
        type            :   String,
        required        :   true
    },
    amount              :   Number,
    files               :   [String],
    type                :   {
        type            :   Schema.Types.ObjectId,
        ref             :   'NeracaType'
    },
    createdAt           :   {
        type            :   Date,
        default         :   Date.now
    },
    user                :   {
        type            :   Schema.Types.ObjectId,
        ref             :   'User'
    }
});

module.exports  =   mongoose.model('Neraca',schema);