var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-paginate');
var Schema              =   mongoose.Schema;

var userSchema = Schema({
    local                   :   {
        email               :   String,
        password            :   String,
        imagePath           :   {
            type            :   String,
            default         :   '/assets/dist/img/user2-160x160.jpg'
        },
        name                :   {
            type            :   String,
            default         :   'Unnamed'
        },
        level               :   {
            type            :   String,
            enum            :   ['Admin', 'Member'],
            default         :   'Member'
        },
        deviceSerialNumber  :   [{
            type            :   Schema.Types.ObjectId,
            ref             :   'Rfid'
        }],
        verified            :   {
            type            :   Boolean,
            default         :   false
        },
        verificationCode    :   {
            type            :   Number,
            select          :   false
        },
        createdAt           :   {
            type            :   Date,
            default         :   Date.now
        }
    },
    facebook                : {
        id                  : String,
        token               : String,
        email               : String,
        name                : String
    },
    twitter                 : {
        id                  : String,
        token               : String,
        displayName         : String,
        username            : String
    },
    google                  : {
        id                  : String,
        token               : String,
        email               : String,
        name                : String
    }
});

userSchema.plugin(mongoosePaginate);

userSchema.methods.addRfidDevice = function(numberID){
    this.local.deviceSerialNumber.push(numberID);
};

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

userSchema.methods.genVerificationCode = function(){
    var high  = 999999,
        low   = 100001;
    return Math.floor(Math.random() * (high - low + 1) + low);
};

userSchema.methods.creationDate = function(){
    var date = new Date(this.local.createdAt);
    return date.getDate();
};

module.exports = mongoose.model('User', userSchema);