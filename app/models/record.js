var mongoose            =   require('mongoose');
var mongoosePaginate    =   require('mongoose-paginate');
var Schema              =   mongoose.Schema;

var schema              =   new Schema({
    rfidCardNumber      :   {
        type            :   String,
        required        :   true
    },
    timeDevice          :   {
        type            :   Date,
        default         :   Date.now
    }
});

schema.plugin(mongoosePaginate);

schema.methods.getTimeDevice = function(){
    var date = new Date(this.local.timeDevice);
    return date.getDate();
};

schema.methods.getTimeServer = function(){
    var date = new Date(this.local.timeServer);
    return date.getDate();
};

module.exports  =   mongoose.model('Record',schema);