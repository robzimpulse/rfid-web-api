var mongoose            =   require('mongoose');
var mongoosePaginate    =   require('mongoose-paginate');
var Schema              =   mongoose.Schema;

var schema              =   new Schema({
    serialNumber  :   {
        type            :   String,
        required        :   true,
        unique          :   true
    },
    records             :   [{
        type            :   Schema.Types.ObjectId,
        ref             :   'Record'
    }]
});

schema.plugin(mongoosePaginate);

module.exports  =   mongoose.model('Rfid',schema);